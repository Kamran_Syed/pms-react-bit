import React, { Component } from 'react';
import { Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import WaitLoading from './WaitLoading.js';


class TabContent extends Component {
	
	constructor(props) {
		super(props);
		this.upwardHandler = this.upwardHandler.bind(this);
	}
	
	upwardHandler(pmsEvent) {
		this.props.upwardEvent(pmsEvent);
	}
	
	render() {
		return (
			<div>
				<WaitLoading show="none" />
				<div>This is TabContent</div>
			</div>
		);
	}
}

export default TabContent;
