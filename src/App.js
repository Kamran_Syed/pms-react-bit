import React, { Component } from 'react';
import { Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import Header from './Header.js';
import Main from './Main.js';
import Footer from './Footer.js';
import WaitLoading from './WaitLoading.js';
import PatientRegistration from './PatientRegistration.js';



class App extends Component {
	
	constructor(props) {
		super(props);
		let tabs = [];
		tabs[0] = {'dataType':null,'show':false};
		tabs[1] = {'dataType':null,'show':false};
		tabs[2] = {'dataType':null,'show':false};
		tabs[3] = {'dataType':null,'show':false};
		
		this.eventHandler = this.eventHandler.bind(this);
		this.state = { 
			mainMenu : {'itemActive' : null},
			secondaryMenu : {'itemActive' : null},
			dateStatus : null,
			dateSelected : null,
			tabs : tabs,
			freeTab : -1,
			
		};
	}
	 
	eventHandler(pmsEvent) {
		console.log(pmsEvent);
		
		switch(pmsEvent.component){
			case "MainMenu":
				this.dispatchMainMenu(pmsEvent);
				break;
			case "SecondaryMenu":
				this.dispatchSecondaryMenu(pmsEvent);
				break;
			case "AppointmentSchedule":
				this.dispatchAppointmentSchedule(pmsEvent);
				break;
			case "AppointmentDetail":
				this.dispatchAppointmentDetail(pmsEvent);
				break;
			default:
			break;
		}
		
	}
	
	getNextTabNumber(){
		let maxTab = 3;
		
		let tab = this.state.freeTab +1;
		if(tab > maxTab){
			tab = maxTab;
		}
		this.state.freeTab = tab;
		
		return tab;
	}
	
	dispatchMainMenu(pmsEvent){
		
		if(pmsEvent.obj.type === 'click'){
			switch(pmsEvent.id){
				case "appointments":
					this.setState({
						mainMenu: {'itemActive' : pmsEvent.id},
						secondaryMenu: {'itemActive' : null}
					});
					break;
				case "settings":
					this.setState({
						mainMenu: {'itemActive' : pmsEvent.id},
						secondaryMenu: {'itemActive' : null}
					});
					break;
				default:
				break;
			}
		}else if(pmsEvent.obj.type === 'react-click'){
			this.setState({
				mainMenu: {'itemActive' : null},
				secondaryMenu: {'itemActive' : null}
			});
		}
	}
	
	dispatchSecondaryMenu(pmsEvent){
		
		if(pmsEvent.obj.type === 'click'){
			switch(pmsEvent.id){
				case "appointments":
					this.setState({
						secondaryMenu: {'itemActive' : pmsEvent.id}
					});
					break;
				case "settings":
					this.setState({
						secondaryMenu: {'itemActive' : pmsEvent.id}
					});
					break;
				default:
				break;
			}
		}
	}
	
	dispatchAppointmentSchedule(pmsEvent){
		console.log("Date Clicked: " + pmsEvent.date.format('YYYY-MM-DD'));
		let tab = this.getNextTabNumber();
		let tempTabs = this.state.tabs;
		tempTabs[tab] = {'dataType':'drAppointmentSchedule','show':true, 'drId':12};
		
		this.setState({
			tabs : tempTabs
		});
	
	}
	
	dispatchAppointmentDetail(pmsEvent) {
		if(pmsEvent.obj.type === 'click'){
			switch(pmsEvent.active){
				case "prescribe":
					//TODO
					break;
				case "reject":
					//TODO
					break;
				default:
				break;
			}
		}
	}
	
	
	render() {
		var jsobj =
		{
		  "2017-02-01":{"status": "available"},
		  "2017-02-02":{"status": "available"},
		  "2017-02-03":{"status": "available"},
		  "2017-02-04":{"status": "available"},
		  "2017-02-09":{"status":"partiallyBooked"},
		  "2017-02-10":{"status":"fullyBooked"}
		  
		};
		
		
		
		return (
			<div className="container">
				<div className="header">
					<Header upwardEvent={this.eventHandler} propsMainMenu={this.state.mainMenu} propsSecondaryMenu={this.state.secondaryMenu} />
				</div>
				
				<div className="footer">
					<Footer  upwardEvent={this.eventHandler} />
				</div>
				<div className="PatientRegistration">
				<PatientRegistration />
				</div>
				<div className="main">
					<Main upwardEvent={this.eventHandler} tabs={this.state.tabs} />
				</div>
			</div>
		);
	}
}

export default App;
