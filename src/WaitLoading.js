import React, { Component } from 'react';
import { Image } from 'react-bootstrap';
import waitGif from './wait.gif';

class WaitLoading extends Component {
  render() {
	  let opclass = "";
	  
	  if(this.props.show === "none"){
		  return null;
	  }else if(this.props.show === "block"){
		  opclass = "waitLoading center-block";
	  }else if(this.props.show === "blur"){
		  opclass = "waitLoading center-block waitLoadingBlur";
	  }else{
		  throw new Error("WaitLoading Failed, wrong props");
	  }
	  
    return (
			<Image className={opclass} src={waitGif} alt="Please Wait" />
    );
  }
}

export default WaitLoading;
