import React, { Component } from 'react';
import './App.css';
import $ from 'jquery';
import { HelpBlock, Button, FormControl, FormGroup ,Glyphicon, Col, Row, Radio, ControlLabel} from 'react-bootstrap';


class PatientRegistration extends Component {
	constructor(props) {
		super(props);
		this.handleChange = this.handleChange.bind(this);
		this.state = {email:null, age:null, ssn:null, gender:null, phone:null};
		this.emailreadyToGo = true;
		this.agereadyToGo = true;
		this.ssnreadyToGo = true;
		this.genderreadyToGo = true;
		this.phonereadyToGo = true;
		this.buttonClick = this.buttonClick.bind(this);
			this.msg = "";
			
	}
		
	 handleChange(e) {
	
	let data = e.target.value;
	let id = e.target.id;
	let vs = null;
	//console.log("id is:" :id);
	//console.log("value is:" :data);
	
	switch(id){
		case "email":
			if(data.length === 0){
				this.setState({ email: null });
				this.emailreadyToGo = false;
				break;
			}
			let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			let res = re.test(data);
			
			if(res === true)
			{
				vs = "success";
				this.emailreadyToGo = true;
			}
			else{
				vs = "error";
				this.emailreadyToGo = false;
			}
			this.setState({ email: vs });
			break;
		case "age":
			if(data.length === 0){
				this.setState({ age: null });
				this.agereadyToGo = false;
				break;
			}
			if((Number.isInteger(Number(data))) && (Number(data)>0) && (Number(data)<150)){
				vs = "success";
				this.agereadyToGo = true;
			}else{
				vs = "error";
				this.agereadyToGo = false;
			}
			this.setState({ age: vs });
			break;
		case "ssn":
			if(data.length === 0){
				this.setState({ ssn: null });
				this.ssnreadyToGo = false;
				break;
			}
			let pattern = /^\d{3}-\d{2}-\d{4}$/;
			let result = pattern.test(data);
			
			if(result === true)
			{
				vs = "success";
				this.ssnreadyToGo = true;
			}
			else{
				vs = "error";
				this.ssnreadyToGo = false;
			}
			this.setState({ ssn: vs });
			break;
		case "phone":
			if(data.length === 0){
				this.setState({ phone: null });
				this.phonereadyToGo = false;
				break;
			}
			let pattrn = /^\(?(\d{3})\)?[- ]?(\d{3})[- ]?(\d{4})$/;
			let reslt = pattrn.test(data);
			
			if(reslt === true)
			{
				vs = "success";
				this.phonereadyToGo = true;
			}
			else{
				vs = "error";
				this.phonereadyToGo = false;
			}
			this.setState({ phone: vs });
			break;
		case "male":
			this.setState({gender: "male"});
			console.log(id);
			break;
		case "female" :
			this.setState({gender: "female"});
			console.log(id);
			break;
		default:
			break;
	}
	
	
    
  }
  
		buttonClick()
	{
		
		if(this.emailreadyToGo===true && this.agereadyToGo===true && this.ssnreadyToGo===true && this.phonereadyToGo===true)
		{
			this.msg = "Email is " + this.email.value + " and First Name is " + this.firstname.value + " and Last Name is " + this.lastname.value + " SSN is " + this.ssn.value + " Age is " + this.age.value + " Gender is " + this.state.gender + " Phone is " + this.phone.value;
			/* this.setState({email:this.email.value});
			this.setState({firstname:this.firstname.value});
			this.setState({lastname:this.lastname.value});
			this.setState({ssn:this.ssn.value});
			this.setState({age:this.age.value}) ; 
			this.setState({gender:this.gender.id}) ; 
			this.setState({phone:this.phone.id}); 
			 */
			alert("hi");
		}
		else
		{
			alert("error please enter again");
		}
		this.getJsonData("http://api.swiftpms.com/rest/patient/");
	}
	
	getJsonData(url){
		let data = {};
		data['email'] = this.email.value;
		data['firstname'] = this.firstname.value;
		data['lasttname'] = this.lastname.value;
		data['ssn'] = this.ssn.value;
		data['age'] = this.age.value;
		data['gender'] = this.state.gender ;
		data['phone'] = this.phone.value;
		
		$.ajax({
		  method: "POST",
		  url: url,
		  data: data
		}).done(function( msg ) {
			this.setState({
				messageType: "success",
				message: "Data Saved",
				clear: true
			});
		}.bind(this)).fail(function( msg ) {
			//do not clear form
			this.setState({
				messageType: "error",
				message: "Cannot Save Data",
				clear: false
			});
			
		}.bind(this));
	}
	
	
	
  render() {
	
	  
    return (
		<div className="Patient_Registration">
				<div className="sft_txt_center"> 
					<h3>Patient Registration </h3> 
				</div> 
				<div className="sft_txt_center">
				<div className="sft_prbox_icon"> 
					 <Glyphicon glyph="user" />
				</div> 
				</div> 
				
				
				<Row> 
					<Col sm={1}>
					</Col>
					<Col sm={10} className="sft_prbox_inner">
						<Row> 
							<Col sm={1}>
							</Col>
							<Col sm={10}>
								<FormGroup controlId="firstname" validationState={this.state.firstname}>
								  <ControlLabel>First Name</ControlLabel>
								  <FormControl type="text" onChange={this.handleChange} placeholder="Enter Your First Name" inputRef={node => this.firstname = node}  />
								  <FormControl.Feedback />
								</FormGroup>
							</Col>
							<Col sm={1}>
							</Col>
						</Row> 
						
						<Row> 
							<Col sm={1}>
							</Col>
							<Col sm={10}>
								<FormGroup controlId="lastname" validationState={this.state.lastname}>
									<ControlLabel>Last Name</ControlLabel>
									<FormControl type="text" onChange={this.handleChange} placeholder="Enter Your Last Name" inputRef={node => this.lastname = node} />
									<FormControl.Feedback />
								</FormGroup>
							</Col>
							<Col sm={1}>
							</Col>
						</Row>
						
						<Row> 
							<Col sm={1}>
							</Col>
							<Col sm={10}>
								<FormGroup controlId="age" validationState={this.state.age}>
									<ControlLabel>Age</ControlLabel>
									<FormControl type="text" onChange={this.handleChange} placeholder="Enter Your Age" inputRef={node => this.age = node} />
									<FormControl.Feedback />
								</FormGroup>
							</Col>
							<Col sm={1}>
							</Col>
						</Row>
						
						<Row> 
							<Col sm={1}>
							</Col>
							<Col sm={10}>
								<FormGroup controlId="ssn" validationState={this.state.ssn}>
									<ControlLabel>Social Security Number</ControlLabel>
									<FormControl type="text" onChange={this.handleChange} placeholder="AAA-GG-SSSS" inputRef={node => this.ssn = node} />
									<FormControl.Feedback />
								</FormGroup>
							</Col>
							<Col sm={1}>
							</Col>
						</Row>
						
						<Row> 
							<Col sm={1}>
							</Col>
							<Col sm={10}>	
								<FormGroup controlId="email" validationState={this.state.email}>
									<ControlLabel>Email</ControlLabel>
									<FormControl type="text" onChange={this.handleChange} placeholder="jsmith@example.com" inputRef={node => this.email = node} />
									<FormControl.Feedback />
								</FormGroup>
							</Col>
							<Col sm={1}>
							</Col>
						</Row>
						
						<Row> 
							<Col sm={1}>
							</Col>
							<Col sm={10}>
								<FormGroup controlId="phone" validationState={this.state.phone}>
									<ControlLabel>Phone</ControlLabel>
									<FormControl type="text" onChange={this.handleChange} placeholder="NXX-NXX-XXXX" inputRef={node => this.phone = node} />
									<FormControl.Feedback />
								</FormGroup>
							</Col>
							<Col sm={1}>
							</Col>
						</Row>
						
						<Row> 
							<Col sm={1}>
							</Col>
							<Col sm={10}>
							<ControlLabel>Gender</ControlLabel>
							</Col>
							<Col sm={1}>
							</Col>
						</Row>
						<Row> 
							<Col sm={1}>
							</Col>
							<Col sm={10}>
								<FormGroup controlId="gender" >
									<Radio inline name="gender" id="male" inputRef={ref => { this.gender = ref; }} onChange={this.handleChange}> Male  </Radio>
									<Radio inline name="gender" id="female" defaultChecked={true} inputRef={ref => { this.gender = ref; }} onChange={this.handleChange}> Female  </Radio>
								</FormGroup>
						</Col>
							<Col sm={1}>
							</Col>
						</Row>
						
						<Row> 
							<Col sm={4}>
							</Col>
							<Col sm={4}>
								<div className="sft_txt_center"> 
									<Button type="submit" bsStyle="primary" onClick={this.buttonClick}>
										Register Now
									</Button>
								</div>
								
							</Col>
							<Col sm={4}>
							</Col>
						</Row>
						<div>{this.msg}</div>
						</Col>
						<Col sm={1}>
						</Col>
				</Row>	
				
		</div>
		
    );
  }
}


export default PatientRegistration;