import React, { Component } from 'react';
import { Button, FormGroup, ControlLabel, FormControl } from 'react-bootstrap';
import WaitLoading from './WaitLoading.js';
import TabContent from './TabContent.js';
import AppointmentSchedule from './AppointmentSchedule.js';
import AppointmentDetail from './AppointmentDetail.js';

class Main extends Component {
//This is now tabs component	

	constructor(props) {
		super(props);
		this.upwardHandler = this.upwardHandler.bind(this);

	}
	
	upwardHandler(pmsEvent) {
		this.props.upwardEvent(pmsEvent);
	}
	
	
	
	render() {
		
		return (
			<div>
				
				<AppointmentDetail upwardEvent={this.upwardHandler}/>
			</div>
		);
	}
}

export default Main;
