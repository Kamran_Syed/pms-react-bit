import React, { Component } from 'react';
import { Calendar, Day } from 'react-calendar';
import { Col, Row, Tooltip, OverlayTrigger } from 'react-bootstrap';
import './calendar.css';

class AppointmentSchedule extends Component {
	
	constructor(props) {
		super(props);
		this.handleClick = this.handleClick.bind(this);
		this.getMods = this.getMods.bind(this);
		this.id = null;
	}
	
	handleClick(date,event){
		let pmsEvent = {};
		pmsEvent['component'] = "AppointmentSchedule";
		pmsEvent['date'] = date;
		
		this.props.upwardEvent(pmsEvent);
	}
	
	getMods() {
		var moment = require('moment');
		let obj = this.props.cal;
		let mods = [];
		
		mods.push(
			{
				date: moment(),
				classNames: [ 'current' ],
				component: [ 'day' ]
			},
			{
				startDate: moment().startOf('month'),
				endDate: moment(),
				classNames: [ 'outside' ],
				component: [ 'day' ]
			},
			{
				component: [ 'day' ],
				events: {
				  onClick: (date,event) => {this.handleClick(date,event)}
				}
			}
		);
				
		for (var key in obj) {
			
		   if (obj.hasOwnProperty(key)) {
				
				switch(obj[key].status) {
					case 'available':
						mods.push(
							{
								date: moment(key),
								classNames: [ 'longEvent' ],
								component: [ 'day' ]
							}
						);
					break;
					case 'partiallyBooked':
						mods.push(
							{
								date: moment(key),
								classNames: [ 'warning' ],
								component: [ 'day' ]
							}
						);
					break;
					case 'fullyBooked':
						mods.push(
							{
								date: moment(key),
								classNames: [ 'booked' ],
								component: [ 'day' ]
							}
						);
					break;
					default:
					break;
				}
			
		    }
		}
		return mods;
	}
	
	render() {
		var moment = require('moment');
		
		return (
			<div>
			<Row className="align-centre">
				<Col lgHidden mdHidden >
					<Calendar 
						date={moment()}
						startDate={moment()}
						endDate={moment().startOf('month')}
						firstMonth={moment().startOf('month')}
						mods={this.getMods()} 
					/>
				</Col>
				<Col xsHidden smHidden >
					<Calendar 
						date={moment()}
						startDate={moment().startOf('month')}
						endDate={moment().add(3, 'months')}
						weekNumbers={false} 
						firstMonth={moment().startOf('month')}
						mods={this.getMods()} 
					/>
				</Col>
			</Row>
			</div>
		);
	}
}

export default AppointmentSchedule;