import React, { Component } from 'react';
import { Button, FormGroup, ControlLabel, FormControl, Row, Col, Image } from 'react-bootstrap';
import WaitLoading from './WaitLoading.js';
import img from './sana-mir-captain.jpg';
import './docDetailedCalendar.css';
import $ from 'jquery';

class AppointmentDetail extends Component {

	constructor(props) {
		super(props);
		this.handleClick = this.handleClick.bind(this);
		this.state = {
			'reject' : false,
			'prescribe' : false,
			'name' : null,
			'status' : null,
			'date' : null,
			'time' : null,
			'desc' : null,
			'isActivePrescribe': 'prescribe',
			'isActiveReject': 'reject'
		};

	}
	
	handleClick(e,obj){
		
		let pmsEvent = {};
		pmsEvent['component'] = "AppointmentDetail";
		pmsEvent['active'] = e;
		pmsEvent['obj'] = obj;
		
		this.props.upwardEvent(pmsEvent);
	}
	
	getJsonData(url){
		
		$.ajax({
		  method: "GET",
		  url: url
		}).done(function( msg ) {
			this.setState({
				messageType: "success",
				message: "Data Retrieved",
				clear: true
			});
			
		this.getAppointments(msg);
		
		}.bind(this)).fail(function( msg ) {
			//do not clear form
			this.setState({
				messageType: "error",
				message: "Cannot Retrieve Data",
				clear: false
			});
			
		}.bind(this));

	}
	
	componentDidMount() {
		
		this.getJsonData("http://api.swiftpms.com/rest/doctor/");
		
	}
	
	getAppointments(msg) {
		let data = msg;
	
		this.setState({
			name: data['name'],
			status: data['status'],
			date: data['appDate'],
			time: data['appTime'],
			desc: data['description']
		});
	}
	
	render() {
		let hide = {"display": "none"};
		let cn = null;
		
		switch(this.state.status) {
			case "available":
				cn = "sp-action-dot bg-green";
			break;
			case "waiting":
				cn = "sp-action-dot bg-yellow";
			break;
			case "not_available":
				cn = "sp-action-dot bg-red";
			break;
			default: 
		}
		
		return (
			<div>
				<WaitLoading show="none" />
				<Row>
					<Col md={12} xs={12} className="sp-action"> 
						<Row>
							<Col xs={3} md={1} >
								<Row>
									<Col xs={12} className="sp-action-img" >
										<Image src={img} circle responsive/>
									</Col>
								</Row>
							</Col>
							<Col xs={9} >
								<Row>
									<Col xs={12} md={4} className="sp-action-details" >
										<span className="sp-action-author" >{this.state.name}</span>
										<p className="sp-action-desc">{this.state.desc}</p>
									</Col>
									<Col xs={4} className="sp-action-datetime">
										<span className="sp-action-date">{this.state.date}</span>
										<span className={cn} ></span>
										<span className="sp=action-time">{this.state.time}</span>
									</Col>
									<Col xs={12} md={4} className="sp-action-buttons">
										<div className="btn-group btn-group-circle">
											<Button type="button" onClick={this.handleClick.bind(this,this.state.isActivePrescribe)} className="btn btn-outline green btn-sm" >Prescribe</Button>
											<Button type="button" onClick={this.handleClick.bind(this,this.state.isActiveReject)} className="btn btn-outline red btn-sm" >Reject</Button>
										</div>
									</Col>
								</Row>
								<Row>
									<Col xs={12} >
										<div style={hide} >This is the description {this.state.data} </div>
									</Col>
								</Row>
							</Col>
						</Row>
					</Col>
				</Row>
			</div>
		);
	}
}

export default AppointmentDetail;