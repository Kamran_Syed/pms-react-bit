import React from 'react';
import ReactDOM from 'react-dom';
import WaitLoading from './WaitLoading.js';

describe('WaitLoading under Microscope', () => {
	
	it('WaitLoading is NOT crashing', () => {
	  const div = document.createElement('div');
	  ReactDOM.render(<WaitLoading show="none" />, div);
	  ReactDOM.render(<WaitLoading show="block" />, div);
	  ReactDOM.render(<WaitLoading show="blur" />, div);
	  
	});
	
	it('WaitLoading Throws error on incorrect props', () => {
		function greenProp() {
			const div = document.createElement('div');
			ReactDOM.render(<WaitLoading show="green" />, div);
		}
		
		expect(greenProp).toThrowError("WaitLoading Failed, wrong props");
	});	
	
});
